package com.example.codewars.challenge

import io.reactivex.Observable
import com.example.codewars.model.AuthoredChallengeResponse
import com.example.codewars.model.CompletedChallengeResponse
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import java.util.concurrent.TimeUnit

interface ChallengeService {

    @GET("{usernameOrId}/code-challenges/completed")
    fun getCompletedChallenges(@Path("usernameOrId") usernameOrId: String, @Query("page") page: Int):
            Observable<CompletedChallengeResponse>

    @GET("{usernameOrId}/code-challenges/authored")
    fun getAuthoredChallenges(@Path("usernameOrId") usernameOrId: String):
            Observable<AuthoredChallengeResponse>

    companion object {
        fun create(): ChallengeService {
            val okHttpClient = OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .build()

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(
                    RxJava2CallAdapterFactory.create())
                .addConverterFactory(
                    GsonConverterFactory.create())
                .baseUrl("https://www.codewars.com/api/v1/users/")
                .client(okHttpClient)
                .build()

            return retrofit.create(ChallengeService::class.java)
        }
    }
}