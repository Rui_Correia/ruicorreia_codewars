package com.example.codewars.challenge

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.example.codewars.R
import com.example.codewars.challenge.fragment.AuthoredChallengesFragment
import com.example.codewars.challenge.fragment.CompletedChallengesFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_challenges.*

class ChallengesActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener  {

    private lateinit var authoredChallengesFragment: AuthoredChallengesFragment
    private lateinit var completedChallengesFragment: CompletedChallengesFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_challenges)

        val username = intent.getStringExtra("username")

        supportActionBar!!.title = "User Challenges"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        bottom_navigation.setOnNavigationItemSelectedListener(this)

        completedChallengesFragment = CompletedChallengesFragment.newInstance(username)
        authoredChallengesFragment = AuthoredChallengesFragment.newInstance(username)

        supportFragmentManager.beginTransaction().add(R.id.main_container, completedChallengesFragment, "1").commit()
        supportFragmentManager.beginTransaction().add(R.id.main_container, authoredChallengesFragment, "2").hide(authoredChallengesFragment).commit()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                finish()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.action_completed_challenges -> {
                supportFragmentManager.beginTransaction().hide(authoredChallengesFragment).show(completedChallengesFragment).commit()
                return true
            }
            R.id.action_authored_challenges -> {
                supportFragmentManager.beginTransaction().hide(completedChallengesFragment).show(authoredChallengesFragment).commit()
                return true
            }
        }
        return false
    }
}