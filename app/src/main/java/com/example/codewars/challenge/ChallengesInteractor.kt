package com.example.codewars.challenge

import com.example.codewars.model.AuthoredChallengeResponse
import com.example.codewars.model.CompletedChallengeResponse

interface ChallengesInteractor {

    interface ChallengesPresenterListener {
        fun onGetCompletedChallengesSuccess(completedChallengeResponse: CompletedChallengeResponse)
        fun onGetAuthoredChallengesSuccess(authoredChallengeResponse: AuthoredChallengeResponse)
        fun onGetChallengesNotFound()
        fun onConnectionError()
    }

    fun getCompletedChallenges(idOrUsername : String, page : Int, listener: ChallengesPresenterListener)
    fun getAuthoredChallenges(idOrUsername : String, listener: ChallengesPresenterListener)
}