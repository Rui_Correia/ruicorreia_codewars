package com.example.codewars.challenge

import com.example.codewars.model.AuthoredChallengeResponse
import com.example.codewars.model.CompletedChallengeResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class ChallengesInteractorImpl(private var challengeService: ChallengeService): ChallengesInteractor {

    private var disposable: Disposable? = null

    override fun getCompletedChallenges(idOrUsername: String, page: Int, listener: ChallengesInteractor.ChallengesPresenterListener) {
        disposable = challengeService.getCompletedChallenges(idOrUsername, page)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {result -> handleCompletedChallengesResult(result, listener)},
                {error -> handleRequestError(error.message, listener)}
            )
    }

    override fun getAuthoredChallenges(idOrUsername: String, listener: ChallengesInteractor.ChallengesPresenterListener) {
        disposable = challengeService.getAuthoredChallenges(idOrUsername)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {result -> handleAuthoredChallengesResult(result, listener)},
                {error -> handleRequestError(error.message, listener)}
            )
    }

    private fun handleCompletedChallengesResult(completedChallengeResponse: CompletedChallengeResponse, listener: ChallengesInteractor.ChallengesPresenterListener) {
        listener.onGetCompletedChallengesSuccess(completedChallengeResponse)
    }

    private fun handleRequestError(message: String?, listener: ChallengesInteractor.ChallengesPresenterListener) {
        if(message != null && message.contains("404")) {
            listener.onGetChallengesNotFound()
        } else {
            listener.onConnectionError()
        }
    }

    private fun handleAuthoredChallengesResult(authoredChallengeResponse: AuthoredChallengeResponse, listener: ChallengesInteractor.ChallengesPresenterListener) {
        listener.onGetAuthoredChallengesSuccess(authoredChallengeResponse)
    }
}