package com.example.codewars.challenge

interface ChallengesPresenter {

    fun getCompletedChallenges(usernameOrId : String, page : Int)
    fun getAuthoredChallenges(usernameOrId : String)
    fun startChallengeDetailsActivity(challengeID : String)
}