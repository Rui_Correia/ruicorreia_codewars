package com.example.codewars.challenge

import com.example.codewars.model.AuthoredChallengeResponse
import com.example.codewars.model.CompletedChallengeResponse

class ChallengesPresenterImpl(var view : ChallengesView?, var challengesInteractor : ChallengesInteractor) : ChallengesPresenter, ChallengesInteractor.ChallengesPresenterListener  {

    override fun onGetCompletedChallengesSuccess(completedChallengeResponse: CompletedChallengeResponse) {
        view?.showCompletedChallenges(completedChallengeResponse.listOfCompletedChallenges, completedChallengeResponse.totalPages)
    }

    override fun onGetAuthoredChallengesSuccess(authoredChallengeResponse: AuthoredChallengeResponse) {
        view?.showAuthoredChallenges(authoredChallengeResponse.listOfAuthoredChallenges)
    }

    override fun onGetChallengesNotFound() {
        view?.showError("Challenges Not Found.", false)
    }

    override fun onConnectionError() {
        view?.showError("Connection Error.", true)
    }

    override fun getCompletedChallenges(usernameOrId: String, page: Int) {
        challengesInteractor.getCompletedChallenges(usernameOrId, page, this)
    }

    override fun getAuthoredChallenges(usernameOrId: String) {
        challengesInteractor.getAuthoredChallenges(usernameOrId, this)
    }

    override fun startChallengeDetailsActivity(challengeID: String) {
        view?.startChallengeDetailsActivity(challengeID)
    }
}