package com.example.codewars.challenge

import com.example.codewars.model.AuthoredChallenge
import com.example.codewars.model.CompletedChallenge

interface ChallengesView {

    fun showCompletedChallenges(completedChallenges: List<CompletedChallenge>, totalPages : Int){}
    fun showAuthoredChallenges(authoredChallenges: List<AuthoredChallenge>){}
    fun showError(message : String, retry : Boolean)
    fun startChallengeDetailsActivity(challengeID: String)
}