package com.example.codewars.challenge.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.example.codewars.R
import com.example.codewars.model.AuthoredChallenge
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import kotlinx.android.synthetic.main.authored_challenge_list_item.view.*

class AuthoredChallengesListAdapter(private val items : ArrayList<AuthoredChallenge>, private val onChallengeClickListener: OnChallengeClickListener, private val context: Context) : RecyclerView.Adapter<AuthoredChallengesListAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.authored_challenge_list_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text = items[position]?.name
        items[position]?.tags.forEach {
            val chip = Chip(holder.chipGroup.context)
            chip.text = it
            chip.isClickable = false
            chip.isCheckable = false
            holder.chipGroup.addView(chip)
        }
        holder.itemView.setOnClickListener {
            onChallengeClickListener.onItemClicked(items[position].id)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }


    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        val name: AppCompatTextView = view.name
        val chipGroup: ChipGroup = view.chips
    }
}

