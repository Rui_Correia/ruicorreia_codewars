package com.example.codewars.challenge.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.example.codewars.R
import com.example.codewars.model.CompletedChallenge
import kotlinx.android.synthetic.main.completed_challenge_list_item.view.*

class CompletedChallengesListAdapter(private val items : ArrayList<CompletedChallenge?>, private val context: Context, private val onChallengeClickListener: OnChallengeClickListener) : RecyclerView.Adapter<CompletedChallengesListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.completed_challenge_list_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text = items[position]?.name
        holder.completedAt.text = items[position]?.completedAt
        holder.itemView.setOnClickListener {
            onChallengeClickListener.onItemClicked(items[position]?.id!!)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        val name: AppCompatTextView = view.name
        val completedAt: AppCompatTextView = view.completedAt
    }
}

