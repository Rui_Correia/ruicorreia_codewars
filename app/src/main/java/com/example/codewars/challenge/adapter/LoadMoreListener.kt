package com.example.codewars.challenge.adapter

/**
 * Load more listener.
 */
interface LoadMoreListener {
    fun onLoadMore()
}