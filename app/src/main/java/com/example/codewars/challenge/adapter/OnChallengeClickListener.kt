package com.example.codewars.challenge.adapter


interface OnChallengeClickListener {
    fun onItemClicked(challengeID : String)
}