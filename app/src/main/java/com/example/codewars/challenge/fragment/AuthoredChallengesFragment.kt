package com.example.codewars.challenge.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.codewars.challenge.adapter.AuthoredChallengesListAdapter
import com.example.codewars.challenge.adapter.OnChallengeClickListener
import com.example.codewars.R
import com.example.codewars.challenge.*
import com.example.codewars.details.DetailsActivity
import com.example.codewars.model.AuthoredChallenge
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_challenges.*

class AuthoredChallengesFragment: Fragment(), ChallengesView, OnChallengeClickListener {

    private val presenter : ChallengesPresenter = ChallengesPresenterImpl(this, ChallengesInteractorImpl(ChallengeService.create()))
    private lateinit var username: String
    private lateinit var challengesList: RecyclerView
    private var listOfChallenge = ArrayList<AuthoredChallenge>()


    companion object {

        /**
         * Function that creates a new instance of the fragment
         *
         * @return the Authored Challenges fragment
         */
        fun newInstance(username : String): AuthoredChallengesFragment {
            val authoredChallengesFragment = AuthoredChallengesFragment()

            val args = Bundle()
            args.putSerializable("username", username)
            authoredChallengesFragment.arguments = args

            return authoredChallengesFragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        username = arguments?.getSerializable("username") as String
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_challenges, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val layoutManager = LinearLayoutManager(activity)
        challengesList = view.findViewById(R.id.challengesList)
        challengesList.layoutManager = layoutManager
        val adapter = AuthoredChallengesListAdapter(listOfChallenge, this, activity as Context)
        challengesList.adapter = adapter
        presenter.getAuthoredChallenges(username)
    }

    override fun showError(message: String, retry : Boolean) {
        val snackbar = Snackbar.make(rootView, message, Snackbar.LENGTH_LONG)
        if(retry) {
            snackbar.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            snackbar.setAction("Retry") {
                presenter.getAuthoredChallenges(username)
            }
        }
        snackbar.show()
    }

    override fun onItemClicked(challengeID: String) {
        presenter.startChallengeDetailsActivity(challengeID)
    }

    override fun showAuthoredChallenges(authoredChallenges: List<AuthoredChallenge>) {
        if(authoredChallenges.isEmpty()) {
            Snackbar.make(rootView, "User has no Authored Challenges.", Snackbar.LENGTH_LONG).show()
        }
        listOfChallenge.addAll(authoredChallenges)
        challengesList.adapter?.notifyDataSetChanged()
    }

    override fun startChallengeDetailsActivity(challengeID: String) {
        val intent = Intent(activity, DetailsActivity::class.java)
        intent.putExtra("challengeID", challengeID)
        startActivity(intent)
    }
}