package com.example.codewars.challenge.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.codewars.R
import com.example.codewars.challenge.*
import com.example.codewars.challenge.adapter.CompletedChallengesListAdapter
import com.example.codewars.challenge.adapter.LoadMoreListener
import com.example.codewars.challenge.adapter.OnChallengeClickListener
import com.example.codewars.details.DetailsActivity
import com.example.codewars.model.CompletedChallenge
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_challenges.*

class CompletedChallengesFragment : Fragment(), ChallengesView, LoadMoreListener, OnChallengeClickListener {
    private val presenter : ChallengesPresenter = ChallengesPresenterImpl(this, ChallengesInteractorImpl(ChallengeService.create()))
    private lateinit var username: String
    private lateinit var challengesList: RecyclerView
    private var listOfChallenge = ArrayList<CompletedChallenge?>()
    private var actualPage = 0

    companion object {
        fun newInstance(username : String): CompletedChallengesFragment {
            val completedChallengesFragment = CompletedChallengesFragment()

            val args = Bundle()
            args.putSerializable("username", username)
            completedChallengesFragment.arguments = args

            return completedChallengesFragment
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_challenges, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        username = arguments?.getSerializable("username") as String
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val layoutManager = LinearLayoutManager(activity)
        challengesList = view.findViewById(R.id.challengesList)
        challengesList.layoutManager = layoutManager
        val adapter = CompletedChallengesListAdapter(listOfChallenge, activity as Context, this)
        challengesList.adapter = adapter

        challengesList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if(layoutManager.findLastVisibleItemPosition() == layoutManager.itemCount-1){
                    onLoadMore()
                }

            }
        })
        presenter.getCompletedChallenges(username, actualPage)
    }

    override fun showCompletedChallenges(completedChallenges: List<CompletedChallenge>, totalPages: Int) {
        if(listOfChallenge.isNotEmpty() && listOfChallenge[listOfChallenge.size -1] == null) {
            listOfChallenge.removeAt(listOfChallenge.size -1)
        }
        listOfChallenge.addAll(completedChallenges)

        if(completedChallenges.isEmpty()) {
            Snackbar.make(rootView, "User has no more Completed Challenges.", Snackbar.LENGTH_LONG).show()
        } else if(totalPages > 1 ) {
            listOfChallenge.add(null)
        }
        challengesList.adapter?.notifyDataSetChanged()
        actualPage++
    }

    override fun showError(message: String, retry: Boolean) {
        val snackbar = Snackbar.make(rootView, message, Snackbar.LENGTH_LONG)
        if(retry) {
            snackbar.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            snackbar.setAction("Retry") {
                presenter.getCompletedChallenges(username, actualPage)
            }
        }
        snackbar.show()
    }

    override fun onItemClicked(challengeID: String) {
        presenter.startChallengeDetailsActivity(challengeID)
    }

    override fun startChallengeDetailsActivity(challengeID: String) {
        val intent = Intent(activity, DetailsActivity::class.java)
        intent.putExtra("challengeID", challengeID)
        startActivity(intent)
    }

    override fun onLoadMore() {
        presenter.getCompletedChallenges(username, actualPage)
    }

}
