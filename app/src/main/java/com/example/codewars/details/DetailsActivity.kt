package com.example.codewars.details

import android.os.Bundle
import android.text.Html
import androidx.appcompat.app.AppCompatActivity
import com.example.codewars.R
import com.example.codewars.model.ChallengeDetails
import com.google.android.material.chip.Chip
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_details.*
import android.view.MenuItem


class DetailsActivity: AppCompatActivity(), DetailsView  {

    private val presenter : DetailsPresenter = DetailsPresenterImpl(this, DetailsInteractorImpl(
        DetailsService.create()))

    private lateinit var challengeID : String

    /**
     * Function that creates the activity.
     *
     * @param savedInstanceState the saved instance state
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        challengeID = intent.getStringExtra("challengeID")

        supportActionBar!!.title = "Challenge Details"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        presenter.getChallengeDetails(challengeID)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                finish()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    /**
     * Shows the challenge details.
     *
     * @param challengeDetails Challenge Details
     */
    override fun showChallengeDetails(challengeDetails: ChallengeDetails) {
        challengeDetails.tags.forEach {
            val chip = Chip(this)
            chip.text = it
            chip.isClickable = false
            chip.isCheckable = false
            tagChips.addView(chip)
        }
        challengeDetails.languages.forEach {
            val chip = Chip(this)
            chip.text = it
            chip.isClickable = false
            chip.isCheckable = false
            languageChips.addView(chip)
        }
        description.text = Html.fromHtml(challengeDetails.description)
        name.text = challengeDetails.name
        category.text = challengeDetails.category
    }


    override fun showError(message: String, retry : Boolean) {
        val snackbar = Snackbar.make(rootView, message, Snackbar.LENGTH_LONG)
        if(retry) {
            snackbar.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            snackbar.setAction("Retry") {
                presenter.getChallengeDetails(challengeID)
            }
        }
        snackbar.show()
    }
}