package com.example.codewars.details

import com.example.codewars.model.ChallengeDetails

interface DetailsInteractor {
    interface DetailsPresenterListener {
        fun onGetChallengeByIDSuccess(challengeDetails: ChallengeDetails)
        fun onChallengeDetailsNotFound()
        fun onConnectionError()
    }
    fun getChallengeDetails(irOrSlug : String, listener: DetailsPresenterListener)
}