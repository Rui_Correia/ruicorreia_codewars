package com.example.codewars.details

import com.example.codewars.model.ChallengeDetails
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class DetailsInteractorImpl (var challengeDetailsService : DetailsService) : DetailsInteractor {

    private var disposable: Disposable? = null

    override fun getChallengeDetails(irOrSlug: String, listener: DetailsInteractor.DetailsPresenterListener) {
        disposable = challengeDetailsService.getChallengeDetails(irOrSlug)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {result -> handleRequestResult(result, listener)},
                {error -> handleRequestError(error.message, listener)}
            )
    }

    private fun handleRequestResult(challengeDetails: ChallengeDetails, listener: DetailsInteractor.DetailsPresenterListener) {
        listener.onGetChallengeByIDSuccess(challengeDetails)
    }

    private fun handleRequestError(message: String?, listener: DetailsInteractor.DetailsPresenterListener) {
        if(message != null && message.contains("404")) {
            listener.onChallengeDetailsNotFound()
        } else {
            listener.onConnectionError()
        }
    }
}