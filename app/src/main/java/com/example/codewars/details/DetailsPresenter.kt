package com.example.codewars.details

interface DetailsPresenter {
    fun getChallengeDetails(irOrSlug:String)
}