package com.example.codewars.details

import com.example.codewars.model.ChallengeDetails

class DetailsPresenterImpl(var view : DetailsView?, var challengesInteractor : DetailsInteractor) : DetailsPresenter, DetailsInteractor.DetailsPresenterListener  {


    override fun getChallengeDetails(irOrSlug : String) {
        challengesInteractor.getChallengeDetails(irOrSlug, this)
    }

    override fun onGetChallengeByIDSuccess(challengeDetails: ChallengeDetails) {
        view?.showChallengeDetails(challengeDetails)
    }

    override fun onChallengeDetailsNotFound() {
        view?.showError("Challenge Details Not Found", false)
    }

    override fun onConnectionError() {
        view?.showError("Connection Error", true)
    }
}