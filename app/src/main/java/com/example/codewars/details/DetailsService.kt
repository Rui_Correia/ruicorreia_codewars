package com.example.codewars.details

import com.example.codewars.model.ChallengeDetails
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface DetailsService {

    @GET("{idOrSlug}")
    fun getChallengeDetails(@Path("idOrSlug") idOrSlug: String):
            Observable<ChallengeDetails>

    companion object {

        fun create(): DetailsService {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(
                    RxJava2CallAdapterFactory.create())
                .addConverterFactory(
                    GsonConverterFactory.create())
                .baseUrl("https://www.codewars.com/api/v1/code-challenges/")
                .build()

            return retrofit.create(DetailsService::class.java)
        }
    }
}