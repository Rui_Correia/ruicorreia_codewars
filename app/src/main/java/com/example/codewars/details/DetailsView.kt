package com.example.codewars.details

import com.example.codewars.model.ChallengeDetails

interface DetailsView {
    fun showChallengeDetails(challengeDetails: ChallengeDetails)
    fun showError(message : String, retry : Boolean)
}