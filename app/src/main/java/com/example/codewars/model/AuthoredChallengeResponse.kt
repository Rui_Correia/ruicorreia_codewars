package com.example.codewars.model

import com.google.gson.annotations.SerializedName

data class AuthoredChallengeResponse (
    @SerializedName("data")
    val listOfAuthoredChallenges : List<AuthoredChallenge>
)