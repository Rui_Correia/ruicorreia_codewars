package com.example.codewars.model

data class ChallengeDetails (
    val approvedAt: String,
    val approvedBy: ApprovedBy,
    val category: String,
    val createdBy: CreatedBy,
    val description: String,
    val id: String,
    val languages: List<String>,
    val name: String,
    val publishedAt: String,
    val rank: Rank,
    val slug: String,
    val tags: List<String>,
    val totalAttempts: Int,
    val totalCompleted: Int,
    val totalStars: Int,
    val url: String
)

data class Rank(
    val color: String,
    val id: Int,
    val name: String
)

data class CreatedBy(
    val url: String,
    val username: String
)

data class ApprovedBy(
    val url: String,
    val username: String
)