package com.example.codewars.model

data class CompletedChallenge (
    val completedAt: String,
    val completedLanguages: List<String>,
    val id: String,
    val name: String,
    val slug: String
)