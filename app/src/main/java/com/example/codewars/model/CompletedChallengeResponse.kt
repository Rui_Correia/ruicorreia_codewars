package com.example.codewars.model

import com.google.gson.annotations.SerializedName

data class CompletedChallengeResponse (
    val totalItems: Int,
    val totalPages: Int,
    @SerializedName("data")
    val listOfCompletedChallenges: List<CompletedChallenge>
)