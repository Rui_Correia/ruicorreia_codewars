package com.example.codewars.model

data class Language (
    var languageName: String?,
    var rank: Int,
    var name: String?,
    var color: String?,
    var score: Int) {

    constructor() : this("",0,"","",0)
}