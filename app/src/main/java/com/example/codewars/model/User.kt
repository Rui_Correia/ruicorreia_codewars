package com.example.codewars.model


import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "Users")
data class User (
    @PrimaryKey
    @ColumnInfo(name = "username")
    @SerializedName("username")
    var username: String,

    @ColumnInfo(name = "name")
    @SerializedName("name")
    var name: String,

    @ColumnInfo(name = "languages")
    var languages: List<Language>?,

    @ColumnInfo(name = "timestamp")
    var timestamp: Date,

    @ColumnInfo(name = "leaderPosition")
    @SerializedName("leaderPosition")
    var leaderPosition: Int) {

    constructor() : this ("","", emptyList(), Date(),0)

    fun getBestLanguage(): Language?{
        var language = Language()
        languages?.forEach {
            if(it.score > language.score){
                language = it
            }
        }

        return language
    }
}