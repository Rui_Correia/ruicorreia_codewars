package com.example.codewars.persistance

import androidx.room.TypeConverter
import com.example.codewars.model.Language
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import java.util.*

class Converters {
    companion object {

        @TypeConverter
        @JvmStatic
        fun fromListOfLanguages(list: List<Language>): String {
            val gson = Gson()
            val json = gson.toJson(list)
            return json
        }

        @TypeConverter
        @JvmStatic
        fun toListOfLanguages(listString: String):  List<Language> {
            val parser = JsonParser()
            var languagesArray = arrayListOf<Language>()
            val languagesObject = parser.parse(listString).asJsonArray
            languagesObject.forEach {
                var language = Language()
                it as JsonObject
                language.languageName = it.get("languageName")?.asString
                if(it.has("rank")) {
                    language.rank = it?.get("rank")?.asInt!!
                }
                language.name = it?.get("name")?.asString
                language.color = it?.get("color")?.asString
                if(it.has("score")) {
                    language.score = it?.get("score")?.asInt!!
                }

                languagesArray.add(language)
            }

            return languagesArray
        }

        @TypeConverter
        @JvmStatic
        fun fromTimestamp(value: Long): Date? {
            return if (value == null) null else Date(value)
        }

        @TypeConverter
        @JvmStatic
        fun dateToTimestamp(date: Date): Long? {
            return if (date == null) null else date!!.time
        }
    }
}