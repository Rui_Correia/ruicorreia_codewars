package com.example.codewars.persistance

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.codewars.model.User
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
interface UserDao {

    /**
     * Gets all users from the db.
     *
     */
    @Query("SELECT * FROM Users")
    fun getAllUsers(): LiveData<List<User>>

    /**
     * Query that inserts the given user in the db.
     * If user already exists it's replaced.
     *
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: User): Completable

    /**
     * Query that gets the user in the db with the given id/username.
     *
     */
    @Query("SELECT * FROM Users WHERE username = :idOrUsername")
    fun getUser(idOrUsername : String) : Flowable<User>

    /**
     * Query that deletes all users in the table.
     */
    @Query("DELETE FROM Users")
    fun deleteAllUsers()

    /**
     * Query that deletes a user in the table.
     */
    @Delete
    fun deleteUser(user: User) : Completable
}