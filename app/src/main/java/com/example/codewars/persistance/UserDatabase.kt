package com.example.codewars.persistance

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.codewars.model.User

@Database(entities = arrayOf(User::class), version = 1)
@TypeConverters(Converters::class)
abstract class UserDatabase : RoomDatabase() {


    abstract fun userDao(): UserDao

    companion object {
        /**
         * Database Instance
         */
        @Volatile private var INSTANCE: UserDatabase? = null

        fun getInstance(context: Context): UserDatabase =
            INSTANCE?: synchronized(this){
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }


        /**
         * Create a new Database
         */
        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext, UserDatabase::class.java, "CodeWarsDB.db").build()
    }
}