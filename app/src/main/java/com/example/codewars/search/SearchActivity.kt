package com.example.codewars.search

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.codewars.R
import com.example.codewars.challenge.ChallengesActivity
import com.example.codewars.model.User
import com.example.codewars.search.adapter.UserListAdapter
import com.example.codewars.utils.Utils
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_search.*

class SearchActivity : AppCompatActivity(), SearchView, View.OnClickListener {

    private val presenter : SearchPresenter = SearchPresenterImpl(this, SearchInteractorImpl(SearchService.create()))
    private var userList = emptyList<User>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        supportActionBar!!.title = "Search Users"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        presenter.getRoomDB()
        presenter.observeUsers()

        val layoutManager = LinearLayoutManager(this)
        searchResultRecycler.layoutManager = layoutManager

        val arrayAdapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arrayOf("Searched Time", "Rank"))
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        sortSpinner.adapter = arrayAdapter
        sortSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when(sortSpinner.adapter.getItem(position)) {
                    "Searched Time" -> {
                        sortUserList(userList)
                    }
                    "Rank" -> {
                        sortUserList(userList)
                    }
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                finish()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onClick(p0: View?) {
        when(p0) {
            searchButton -> {
                if(searchText.text.toString() == "") {
                    showError("Query should not be empty.", false)
                } else {
                    presenter.getUser(searchText.text.toString())
                }
            }
        }
    }

    override fun showUsers(usersList: List<User>) {
        val adapter = UserListAdapter(ArrayList(usersList), this)
        searchResultRecycler.adapter = adapter
    }

    override fun showError(message: String, retry : Boolean) {
        val snackbar = Snackbar.make(rootView, message, Snackbar.LENGTH_LONG)
        if(retry) {
            snackbar.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            snackbar.setAction("Retry") {
                presenter.getUser(searchText.text.toString())
            }
        }
        snackbar.show()
    }

    override fun startChallengesActivity(user: User) {
        val intent = Intent(this, ChallengesActivity::class.java)
        intent.putExtra("username", user.username)
        startActivity(intent)
    }

    private fun sortUserList(usersList: List<User>){
        userList = if(sortSpinner.selectedItem == "Searched Time") {
            Utils.sortUserListByTimeOrRank(usersList, true)
        } else {
            Utils.sortUserListByTimeOrRank(usersList, false)
        }
        val adapter = UserListAdapter(ArrayList(userList), this)
        searchResultRecycler.adapter = adapter
    }
}