package com.example.codewars.search

import android.content.Context
import androidx.lifecycle.LiveData
import com.example.codewars.model.User

interface SearchInteractor {

    interface SearchPresenterListener {
        fun onGetUserSuccess(user: User)
        fun onUserInsertedWithSuccess(user: User)
        fun onUserNotFound()
        fun onConnectionError()
    }

    fun getUser(usernameOrId: String, listener: SearchPresenterListener)
    fun getRoomDB(context: Context)
    fun getAllUsers() : LiveData<List<User>>
    fun replaceUserOnDB(oldestUser: User, newUser: User, listener: SearchPresenterListener)
    fun saveUserOnDB(user: User, listener: SearchPresenterListener)
}