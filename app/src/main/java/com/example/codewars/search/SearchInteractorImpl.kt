package com.example.codewars.search

import android.content.Context
import androidx.lifecycle.LiveData
import com.example.codewars.model.User
import com.example.codewars.persistance.UserDao
import com.example.codewars.persistance.UserDatabase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class SearchInteractorImpl(var searchService: SearchService) : SearchInteractor {


    private var disposable: Disposable? = null
    private lateinit var userDao: UserDao
    private lateinit var allUsers: LiveData<List<User>>

    override fun getUser(usernameOrId: String, listener: SearchInteractor.SearchPresenterListener) {
        disposable = searchService.getUser(usernameOrId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {result -> handleSearchResult(result, listener)},
                {error -> handleSearchError(error.message, listener)}
            )
    }

    override fun getRoomDB(context: Context) {
        val userDatabase = UserDatabase.getInstance(context)
        userDao = userDatabase.userDao()
        allUsers = userDao.getAllUsers()
    }

    /**
     * Handles the search result.
     */
    private fun handleSearchResult(result: User, listener: SearchInteractor.SearchPresenterListener) {
        listener.onGetUserSuccess(result)
    }

    /**
     * Handles the search response error.
     *
     */
    private fun handleSearchError(message: String?, listener: SearchInteractor.SearchPresenterListener) {
        if(message != null && message.contains("404")) {
            listener.onUserNotFound()
        } else {
            listener.onConnectionError()
        }
    }

    override fun replaceUserOnDB(oldestUser: User, newUser: User, listener: SearchInteractor.SearchPresenterListener) {
        disposable = userDao.deleteUser(oldestUser)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {listener.onUserInsertedWithSuccess(newUser)},
                {error -> handleSearchError(error.message, listener)}
            )
    }

    override fun getAllUsers(): LiveData<List<User>> {
        return allUsers
    }

    override fun saveUserOnDB(user: User, listener: SearchInteractor.SearchPresenterListener) {
        disposable = userDao.insertUser(user)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {},
                {error -> handleSearchError(error.message, listener)}
            )
    }
}