package com.example.codewars.search

interface SearchPresenter {

    fun getRoomDB()
    fun getUser(usernameOrId : String)
    fun observeUsers()
}