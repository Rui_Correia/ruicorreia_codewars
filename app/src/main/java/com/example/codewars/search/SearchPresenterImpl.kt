package com.example.codewars.search

import android.content.Context
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.example.codewars.model.User
import com.example.codewars.utils.Utils

class SearchPresenterImpl (var view: SearchView?, var searchInteractor: SearchInteractor): SearchPresenter, SearchInteractor.SearchPresenterListener{

    override fun onGetUserSuccess(user: User) {
        val userList = searchInteractor.getAllUsers().value
        if(userList!=null){
            if (userList.size < 5 || Utils.contains(userList, user)) {
                searchInteractor.saveUserOnDB(user, this)
            } else {
                val oldestUser = Utils.getOldestUser(userList)
                if (oldestUser != null) {
                    searchInteractor.replaceUserOnDB(oldestUser, user, this)
                }
            }
        }
    }

    override fun onUserInsertedWithSuccess(user: User) {
        searchInteractor.saveUserOnDB(user, this)
    }

    override fun onUserNotFound() {
        view?.showError("User Not Found", false)
    }

    override fun onConnectionError() {
        view?.showError("Connection Error", true)
    }

    /**
     * Get room DB instance
     */
    override fun getRoomDB() {
        searchInteractor.getRoomDB(view as Context)
    }

    /**
     * Get the user
     */
    override fun getUser(usernameOrId: String) {
        searchInteractor.getUser(usernameOrId, this)
    }

    /**
     * Criates an observer on users list LiveData.
     */
    override fun observeUsers() {
        searchInteractor.getAllUsers().observe(view as LifecycleOwner, Observer { users ->
            users?.let {
                view?.showUsers(users)
            }
        })
    }
}