package com.example.codewars.search

import com.example.codewars.model.User
import com.google.gson.GsonBuilder
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface SearchService {

    @GET("users/{usernameOrId}")
    fun getUser(@Path("usernameOrId") usernameOrId: String):
            Observable<User>

    companion object {

        fun create(): SearchService {

            val gsonBuilder = GsonBuilder()
            gsonBuilder.registerTypeAdapter(User::class.java, UserDeserializer())
            val gson = gsonBuilder.create()

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(
                    RxJava2CallAdapterFactory.create())
                .addConverterFactory(
                    GsonConverterFactory.create(gson))
                .baseUrl("https://www.codewars.com/api/v1/")
                .build()

            return retrofit.create(SearchService::class.java)
        }
    }
}