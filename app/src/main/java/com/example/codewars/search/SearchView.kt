package com.example.codewars.search

import com.example.codewars.model.User

interface SearchView {
    fun showUsers(usersList : List<User>)
    fun showError(message : String, retry : Boolean)
    fun startChallengesActivity(user: User)
}