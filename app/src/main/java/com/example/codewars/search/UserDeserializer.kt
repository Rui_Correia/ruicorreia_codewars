package com.example.codewars.search

import com.example.codewars.model.Language
import com.example.codewars.model.User
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type

class UserDeserializer : JsonDeserializer<User> {


    /**
     * Deserialize the getUser response
     */
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): User {
        val root = json?.asJsonObject
        val user = User()

        val username = root?.get("username")?.asString!!
        user.username = username
        if(!root.get("name").isJsonNull) {
            user.name = root.get("name").asString
        }
        if(!root.get("leaderboardPosition").isJsonNull) {
            user.leaderPosition = root.get("leaderboardPosition").asInt
        }

        var languagesArray = arrayListOf<Language>()
        val languagesObject = root.getAsJsonObject("ranks").getAsJsonObject("languages")
        languagesObject.keySet().forEach{
            val language = Language()
            val languageObject = languagesObject.getAsJsonObject(it)
            language.languageName = it
            if(!languageObject.get("rank").isJsonNull) {
                language.rank = languageObject?.get("rank")?.asInt!!
            }
            language.name = languageObject?.get("name")?.asString
            language.color = languageObject?.get("color")?.asString
            if(!languageObject.get("score").isJsonNull) {
                language.score = languageObject?.get("score")?.asInt!!
            }

            languagesArray.add(language)
        }
        user.languages = languagesArray

        return user
    }

}