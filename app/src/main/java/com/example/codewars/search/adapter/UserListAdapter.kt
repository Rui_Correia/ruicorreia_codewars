package com.example.codewars.search.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.example.codewars.R
import com.example.codewars.model.User
import com.example.codewars.search.SearchView
import kotlinx.android.synthetic.main.user_list_item.view.*


class UserListAdapter(private val items : ArrayList<User>, private val searchView: SearchView) : RecyclerView.Adapter<UserListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(searchView as Context).inflate(R.layout.user_list_item, parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            searchView.startChallengesActivity(items[position])
        }
        holder.username.text = items[position].username
        holder.rank.text = "${items[position].leaderPosition}"
        holder.bestLanguage.text = "${items[position].getBestLanguage()!!.languageName} (${items[position].getBestLanguage()!!.score} points)"
    }


    override fun getItemCount(): Int {
        return items.size
    }

    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        val username: AppCompatTextView = view.username
        val rank: AppCompatTextView = view.rank
        val bestLanguage: AppCompatTextView = view.bestLanguage
    }
}

