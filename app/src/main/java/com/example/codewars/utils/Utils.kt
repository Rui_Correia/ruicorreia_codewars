package com.example.codewars.utils

import com.example.codewars.model.User
import java.util.*

class Utils {
    companion object {

        /**
         * Function that returns the oldest user from the list.
         */
        fun getOldestUser(userList : List<User>) : User? {
            var auxDate = Date()
            var user = User()
            userList.forEach {
                if(it.timestamp < auxDate) {
                    auxDate = it.timestamp
                    user = it
                }
            }
            return user
        }

        /**
         * Function that sorts the users list by time or rank.
         */
        fun sortUserListByTimeOrRank(usersList: List<User>, time: Boolean) : List<User> {
            return if(time) {
                usersList.sortedWith(compareByDescending { it.timestamp })
            } else {
                usersList.sortedWith(compareBy { it.leaderPosition })
            }
        }

        /**
         * Function that checks if the users list contains the given user.
         */
        fun contains(usersList: List<User>, user: User) : Boolean {
            usersList.forEach {
                if(it.username == user.username) {
                    return true
                }
            }
            return false
        }
    }
}